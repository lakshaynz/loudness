FROM python:alpine
RUN pip install numpy colorama
COPY loud.py .
CMD python loud.py
