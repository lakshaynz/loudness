import pyaudio
import numpy as np
import os
import time
import ctypes
from colorama import Fore, Back, init

# Initialize colorama
init(autoreset=True)

# Parameters
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
CHUNK = 1024
MAX_AMPLITUDE = 32767  # For int16
BAR_MAX_LENGTH = 50  # Maximum number of characters for the bar

# Initialize PyAudio
audio = pyaudio.PyAudio()

# Start streaming audio
stream = audio.open(format=FORMAT, channels=CHANNELS,
                    rate=RATE, input=True,
                    frames_per_buffer=CHUNK)

print("Reading Audio Values. Press Ctrl+C to stop.")

def create_bar(amplitude, max_amplitude, bar_max_length):
    bar_length = int((amplitude / max_amplitude) * bar_max_length)
    return bar_length

def amplitude_to_db(amplitude):
    # Avoid log(0) by adding a small constant
    return 20 * np.log10((amplitude + np.finfo(float).eps) / 32767.0)

def display_alert(message, title):
    ctypes.windll.user32.MessageBoxW(0, message, title, 1)

max_seen_amplitude = 0
amplitude_values = [np.abs(np.random.randn() * 0.1 * MAX_AMPLITUDE) for _ in range(300)]
LOUDNESS_THRESHOLD = 15000

try:
    while True:
        # Read audio stream
        data = stream.read(CHUNK, exception_on_overflow=False)
        int_values = np.frombuffer(data, dtype=np.int16)  # Convert byte data to integers
        avg_amplitude = np.abs(int_values).mean()  # Compute average amplitude
        amplitude_values.append(avg_amplitude)
        amplitude_values.pop(0)

        amplitude_values = [np.abs(np.random.randn() * 0.1 * MAX_AMPLITUDE) for _ in range(300)]

        avg_db = amplitude_to_db(avg_amplitude)
        max_seen_db = amplitude_to_db(max_seen_amplitude)
        db_values = [amplitude_to_db(np.abs(np.random.randn() * 0.1 * MAX_AMPLITUDE)) for _ in range(300)]

        # Update max_seen_amplitude
        if avg_amplitude > max_seen_amplitude:
          max_seen_amplitude = avg_amplitude

        if avg_amplitude > LOUDNESS_THRESHOLD:
            display_alert("WARNING: You're being too loud!", "Loudness Alert")
        
        # Create the audio bars
        current_bar_length = create_bar(avg_amplitude, MAX_AMPLITUDE, BAR_MAX_LENGTH)
        max_bar_length = create_bar(max_seen_amplitude, MAX_AMPLITUDE, BAR_MAX_LENGTH)
        
        # Clear the terminal
        os.system('cls' if os.name == 'nt' else 'clear')
        
        # Print the audio bars
        print("Current Volume: " + Fore.GREEN + "#" * current_bar_length)
        print("Maximum Volume: " + Fore.RED + "#" * max_bar_length)
        # Print max amplitude value
        print("Max amp (raw): " + Fore.GREEN + str(round(max_seen_amplitude, 2)))

        # Introduce a delay for readability
        time.sleep(0.1)

except KeyboardInterrupt:
    pass
finally:
    # Stop and close the audio stream
    stream.stop_stream()
    stream.close()
    audio.terminate()
    print("\nStopped Reading Audio.")
